#include <switch.h>
#include <precisionui.h>
int main(int argc, char **argv) {
  PUI_Init();
  PUI_DrawDividers(true);
  PUI_DrawTitle(true);
  PUI_SetTitle("PrecisionUI Example");
  ColorSetId theme;
  setsysGetColorSetId(&theme);
  while (appletMainLoop()) {
    PUI_Update();
    PUI_Element_Sidebar(3, "Home", "~~~DIVIDER~~~", "Other Tab");
    u32 touch_count = hidTouchCount();

    char buf[64] = "placeholder";
    sprintf(buf, "Theme: %s", theme == ColorSetId_Dark ? "dark" : "light");
    PUI_Element_Divider();
    PUI_Element_Label(InterUI_Regular, buf, true);
    PUI_Element_Divider();
    int exitBtn = PUI_Element_Button(InterUI_Regular, "Exit", true);
    PUI_Element_Divider();
    int elemTouching = PUI_Touch_Element_Touching(touch_count);
    bool exitFlag = PUI_KeyScan(KEY_PLUS);
    PUI_Element exitBtnEl = ELEMENTS[exitBtn];
    if (exitBtnEl.tapped || exitFlag) break;
    PUI_Present();
  }
  PUI_Close();
  return 0;
}